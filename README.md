# README #

# Ninja Dashboard setup and configuration #

![Dashboard.JPG](https://bitbucket.org/repo/dp4E6r/images/1174427166-Dashboard.JPG)

### Created by Brian Drake ###

The purpose of this page is to help with the installation and setup of a team Dashboard that I have created for Team Ninja. 

## What you will need to get started: ##
1. Apache Tomcat server running locally.
2. Get the newest version of the source code from the Git repository or Download this [.WAR file](https://bitbucket.org/Drakun/dashboard/src/bc26eddb6a489d54687759ddf76e211c6d7b4d70/NinjaDashboard.war?at=master).
3. Copy the [Config file](https://Drakun@bitbucket.org/Drakun/dashboard/src/390738b27a95c895f1ed56713c564dd31bc46c7d/Config?at=master).
 
## Installation Step 1: Create folder for the config file
Create a folder in this path: "c:/Dashboard_Config" and place the config file in that folder.

## Installation Step 2: Add .WAR file to the Apache server
Either create a .WAR file using the downloaded source code or download the .WAR file directly. Once you have the .WAR file, unzip the Apache server and open the folder. Place the .WAR file in the "webapp" folder.

## Installation Step 3: Startup the Apache server
Open up a Command Prompt and traverse to the Apache directory. Once there, go into the "/bin" folder and type "startup.bat". If you get an Error specifying that you need your JAVA_HOME setup in your class path then make sure you initialize that and try again. You will need to restart your command prompt.

## Installation Step 4: Open the page
You should now be able to open your browser and go to "localhost:8080/NinjaDashboard/". You may also want to modify the config file that you placed in your c:/ directory to reflect your teams metrics and name.
Logging on: Enter your JIRA User Name and Password, you will also need your Sprint ID

In order to get your sprint ID, open JIRA up in your browser and navigate to your teams scrum board and click on "Reports". In the HTML header your will see the sprint ID.

![sprint ID.JPG](https://bitbucket.org/repo/dp4E6r/images/2989817100-sprint%20ID.JPG)

In the above screen shot you can see the sprint ID is "1973". Once this information is added and you press the submit button, your teams scrum board should load.