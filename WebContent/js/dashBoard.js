/**
 * Created by bdrake on 7/8/2015.
 */

function updateClock ( )
{
    var currentTime = new Date ( );
    var currentHours = currentTime.getHours ( );
    var currentMinutes = currentTime.getMinutes ( );
    var currentSeconds = currentTime.getSeconds ( );
    var year = currentTime.getFullYear();

    // Pad the minutes and seconds with leading zeros, if required
    currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
    currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

    // Choose either "AM" or "PM" as appropriate
    var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";

    // Convert the hours component to 12-hour format if needed
    currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;

    // Convert an hours component of "0" to "12"
    currentHours = ( currentHours == 0 ) ? 12 : currentHours;

    // Compose the string for display
    var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;


    $("#time").html(currentTimeString);
    $("#year").html(year);
}
$(document).ready(function()
{
    setInterval('updateClock()', 1000);
    setInterval('updateBuild()', 10000);
    setInterval('getStories()', 60000);
    setInterval('getCompleted()', 60000);
    setInterval('graph()', 60000);
    setInterval('timeGraph()', 60000);
});

function getName()
{
	$.get('http://localhost:8080/NinjaDashboard/name', function(data) 
			{
				data = data.replace(/[\[\]']+/g,'');
				var array = data.split(",");
				
				document.getElementById('acronym').innerHTML = array[0];
				document.getElementById('title').innerHTML = array[1];
				document.getElementById('teamTitle').innerHTML = array[1];
			});
}

function updateBuild()
{
	//document.location.href="http://localhost:8080/NinjaDashboard/DashBoard";
	var servers = new Array();
	
	for(i = 1; i < 7; i++)
	{
		$.ajax({
			url: 'http://localhost:8080/NinjaDashboard/server?num=' + i, 
			async: false,
		}).done(function(server) 
				{
					servers[i-1] = server;
				});
	}
	
	document.getElementById('server1').innerHTML = servers[0];
	document.getElementById('server2').innerHTML = servers[1];
	document.getElementById('server3').innerHTML = servers[2];
	document.getElementById('server4').innerHTML = servers[3];
	document.getElementById('server5').innerHTML = servers[4];
	document.getElementById('server6').innerHTML = servers[5];
	
	$.get('http://localhost:8080/NinjaDashboard/DashBoard', function(data) 
			{
				data = data.replace(/[\[\]']+/g,'');
				var array = data.split(",");
				var string1; 
				var string2;
		        //alert(array[i]);
				
				for(i = 0; i < array.length; i++)
					{
					//alert("Array: " + array[i] + " Server: " + ' ' + servers[0]);
					string1 = array[i].trim();
					//alert(string1 + " " + string2);
						switch(string1)
						{
						case servers[0]:
							{
								switch(array[i+1])
								{
								case ' notbuilt':
									notbuilt('Aries_Build');
									i++;
									break;
								case ' blue_anime':
									anime('Aries_Build');
									i++;
									break;
								case ' red_anime':
									anime('Aries_Build');
									i++;
									break;
								case ' blue':
									blue('Aries_Build');
									i++;
									break;
								case ' red':
									red('Aries_Build');
									i++;
									break;
								default:
									i++;
									white('Aries_Build');	
								}
							}
							break;
						case servers[1]:
							{
								switch(array[i+1])
								{
								case ' notbuilt':
									notbuilt('Aries_Merge');
									i++;
									break;
								case ' blue_anime':
									anime('Aries_Merge');
									i++;
									break;
								case ' red_anime':
									anime('Aries_Merge');
									i++;
									break;
								case ' blue':
									blue('Aries_Merge');
									i++;
									break;
								case ' red':
									red('Aries_Merge');
									i++;
									break;
								default:
									i++;
									white('Aries_Merge');
								}
							}
							break;
						case servers[2]:
							{
								switch(array[i+1])
								{
								case ' notbuilt':
									i++;
									notbuilt('Axi_Build');
									break;
								case ' blue_anime':
									i++;
									anime('Axi_Build');
									break;
								case ' red_anime':
									i++;
									anime('Axi_Build');
									break;
								case ' blue':
									i++;
									blue('Axi_Build');
									break;
								case ' red':
									i++;
									red('Axi_Build');
									break;
								default:
									i++;
									white('Axi_Build');	
								}
							}
							break;
						case servers[3]:
							{
								switch(array[i+1])
								{
								case ' notbuilt':
									notbuilt('Axi_Merge');
									i++;
									break;
								case ' blue_anime':
									anime('Axi_Merge');
									i++;
									break;
								case ' red_anime':
									anime('Axi_Merge');
									i++;
									break;
								case ' blue':
									blue('Axi_Merge');
									i++;
									break;
								case ' red':
									red('Axi_Merge');
									i++;
									break;
								default:
									i++;
									white('Axi_Merge');
								}
							}
							break;
						case servers[4]:
							{
								switch(array[i+1])
								{
								case ' notbuilt':
									notbuilt('Av-Web_Build');
									i++;
									break;
								case ' blue_anime':
									anime('Av-Web_Build');
									i++;
									break;
								case ' red_anime':
									anime('Av-Web_Build');
									i++;
									break;
								case ' blue':
									blue('Av-Web_Build');
									i++;
									break;
								case ' red':
									red('Av-Web_Build');
									i++;
									break;
								default:
									i++;
									white('Av-Web_Build');	
								}
							}
							break;
						case servers[5]:
							{
								switch(array[i+1])
								{
								case ' notbuilt':
									notbuilt('Av-Web_Merge');
									i++;
									break;
								case ' blue_anime':
									anime('Av-Web_Merge');
									i++;
									break;
								case ' red_anime':
									anime('Av-Web_Merge');
									i++;
									break;
								case ' blue':
									blue('Av-Web_Merge');
									i++;
									break;
								case ' red':
									red('Av-Web_Merge');
									i++;
									break;
								default:
									i++;
									white('Av-Web_Merge');
								}
							}
							break;
						}
					}
			});
}

function getStories()
{
	$.get('http://localhost:8080/NinjaDashboard/stories', function(data) 
			{
				data = data.replace(/[\[\]']+/g,'');
				var array = data.split(",");
				value = array.join("");
				document.getElementById('stories').innerHTML = value;
				
				$.get('http://localhost:8080/NinjaDashboard/activePoints', function(activePoints) 
						{
							document.getElementById('activePoints').innerHTML = activePoints;
						});
			});
}
function getCompleted()
{
	$.get('http://localhost:8080/NinjaDashboard/completed', function(data) 
			{
				data = data.replace(/[\[\]']+/g,'');
				var array = data.split(",");
				value = array.join("");
				document.getElementById('completed').innerHTML = value;
				
				$.get('http://localhost:8080/NinjaDashboard/completedPoints', function(CompletedPoints) 
						{
							document.getElementById('completedPoints').innerHTML = CompletedPoints;
						});
			});
}

function graph() 
{
	$.get('http://localhost:8080/NinjaDashboard/activePoints', function(activePoints) 
			{
				var total = 0;
				var average = 0 ;
				var active = activePoints;
				$.get('http://localhost:8080/NinjaDashboard/completedPoints', function(CompletedPoints) 
						{
							var complete = CompletedPoints;
							
							total = parseInt(active) + parseInt(complete);
							average = (complete/total)*100;
							
							var rp2 = radialProgress(document.getElementById('graph'))
						    .diameter(220)
						    .value(average)
						    .label("BurnDown Completion")
						    .render();
								
						});
				
			});
}
function timeGraph() 
{
	$.get('http://localhost:8080/NinjaDashboard/time', function(time) 
		{
			var rp1 = radialProgress2(document.getElementById('sprintTime'))
		    .diameter(220)
		    .value(time)
		    .label("Time Remaining")
		    .render();
		});
}

function switchView()
{
	if((document.getElementById('graphs').style.display) == "none")
		{
			document.getElementById('graphs').style.display = "block";
			document.getElementById('completedBox').style.display = "none";
			graph();
			timeGraph();
		}
	else
		{
			document.getElementById('graphs').style.display = "none";
			document.getElementById('completedBox').style.display = "block";
		}
}

function notbuilt(name)
{
	//document.getElementById(name).style.backgroundColor = '#f2dede';
	document.getElementById(name).style.backgroundColor = '#b21909';
}
function anime(name)
{
	//document.getElementById(name).style.backgroundColor = '#fcf8e3';
	document.getElementById(name).style.backgroundColor = '#ffda00';
}
function blue(name)
{
	//document.getElementById(name).style.backgroundColor = '#dff0d8';
	document.getElementById(name).style.backgroundColor = '#02a17c';
}
function red(name)
{
	//document.getElementById(name).style.backgroundColor = '#f2dede';
	document.getElementById(name).style.backgroundColor = '#b21909';
}
function white(name)
{
	document.getElementById(name).style.backgroundColor = 'white';
	document.getElementById(name).style.color = '#4d4f53';
}









