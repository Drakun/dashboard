package javaServlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ConnectServlet extends HttpServlet 
{
	private static JiraConnection stories;
	GetConfig configData = new GetConfig();

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String userName = request.getParameter("userName");
		String password = request.getParameter("password");
		String sprint = request.getParameter("sprint");
		stories = new JiraConnection(userName,password,sprint);
		
		String url = configData.getLandingPage();
		response.sendRedirect(url);
	}
	public static JiraConnection getConnection()
	{
		return stories;
	}

}
