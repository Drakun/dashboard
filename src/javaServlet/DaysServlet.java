package javaServlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DaysServlet extends HttpServlet 
{
	private JiraConnection stories = ConnectServlet.getConnection();
	GetConfig configData = new GetConfig();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		try
		{
			stories = ConnectServlet.getConnection();
			stories.connect();
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			
			out.print(stories.getDays());
		}
		catch(IOException e){
		}catch(Exception p){
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String url = configData.getLandingPage();
		response.sendRedirect(url);
	}

}
