package javaServlet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GetConfig 
{
	private JSONObject jsonObj;
	
	public GetConfig()
	{
		File config = new File("C:/Dashboard_Config/Config"); //current directory
	    String path = config.getAbsolutePath();
	    System.out.println(path);
	    
	    BufferedReader in;
		try 
		{
			in = new BufferedReader(new FileReader(config));
			String input = in.readLine();
			String data = input;
			while(input != null)
		    {
		    	input = in.readLine();
		    	data += input;
		    }
			
			jsonObj = new JSONObject(data);
			in.close();
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	    
	    //System.out.println(jsonObj.getInt("Version"));
	}
	
	public String getLandingPage()
	{
		try {
			return jsonObj.getString("landing_page");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String getJenkinsUrl()
	{
		try {
			return jsonObj.getString("jenkinsApiUrl");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String getServer(String num)
	{
		try 
		{
			return jsonObj.getString("server" + num);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String getJiraUrl()
	{
		try 
		{
			return jsonObj.getString("jira_connection");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String getName()
	{
		try 
		{
			return jsonObj.getString("team_acronym") + "," + jsonObj.getString("team_title");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String getNameAlert()
	{
		try 
		{
			return jsonObj.getString("name_alert");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
