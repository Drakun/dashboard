package javaServlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import javax.net.ssl.X509TrustManager;


public class JiraConnection 
{
	private GetConfig configData = new GetConfig();
	private ArrayList active = new ArrayList();
	private ArrayList completed = new ArrayList();
	private String name;
	private String password;
	private String sprint;
	String result = null;
	private double days = 0.0;
	
	public JiraConnection()
	{
		setName(null);
		setPassword(null);
		setSprint(null);
	}
	public JiraConnection(String user, String password, String sprint)
	{
		setName(user);
		setPassword(password);
		setSprint(sprint);
	}
	public void connect() throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, JSONException
	{	
		// Create a trust manager that does not validate certificate chains
		final TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
	        @Override
	        public void checkClientTrusted( final X509Certificate[] chain, final String authType ) {
	        }
	        @Override
	        public void checkServerTrusted( final X509Certificate[] chain, final String authType ) {
	        }
	        @Override
	        public X509Certificate[] getAcceptedIssuers() {
	            return null;
	        }
	    } };

		// Install the all-trusting trust manager
		try {
		    SSLContext sc = SSLContext.getInstance("SSL"); 
		    sc.init(null, trustAllCerts, new java.security.SecureRandom()); 
		    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} 
		// Now you can access an https URL without having the certificate in the truststore
		try { 
			String webPage = configData.getJiraUrl() + sprint + "&maxResults=100";
			String authString = name + ":" + password;

			byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
			String authStringEnc = new String(authEncBytes);

			URL url = new URL(webPage);
			//HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
			URLConnection urlConnection = url.openConnection();
			//con.setRequestProperty("Authorization", "Basic " + authStringEnc);
			urlConnection.setRequestProperty("Authorization", "Basic " + authStringEnc);
			//InputStream is = con.getInputStream();
			InputStream is = urlConnection.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
	
			int numCharsRead;
			char[] charArray = new char[1024];
			StringBuffer sb = new StringBuffer();
			while ((numCharsRead = isr.read(charArray)) > 0) {
				sb.append(charArray, 0, numCharsRead);
			}
			result = sb.toString();
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} 		
	}
	public String getActive()
	{
		try {
			JSONObject stories = new JSONObject(result);
			JSONArray issues = stories.getJSONArray("issues");

			active.clear();
			String display = null;
			String story = null;
			String status = null;
			String summary = null;
			String assigned = null;
			String nameAlert = configData.getNameAlert();
			int storyPoint = 0;
			
			for(int i = 0; i < issues.length(); i++)
			{
				story = issues.getJSONObject(i).getJSONObject("fields").getJSONObject("issuetype").getString("name");
				status = issues.getJSONObject(i).getJSONObject("fields").getJSONObject("status").getString("name");
				summary = issues.getJSONObject(i).getJSONObject("fields").getString("summary");
				
				if(story.equals("Story") && !status.equals("Done Done"))
				{
					if(!issues.getJSONObject(i).getJSONObject("fields").isNull("customfield_10002"))
					{
						storyPoint = issues.getJSONObject(i).getJSONObject("fields").getInt("customfield_10002");
					}
					try
					{
						assigned = issues.getJSONObject(i).getJSONObject("fields").getJSONObject("assignee").getString("displayName");
						if(assigned.equals(nameAlert))
						{
							assigned = "<strong><span id='nameAlert' style='float:right;'>" + assigned + "</span></strong>";
						}
						
						display = "<p style='float:left;width:60%'><strong><span style='color:#3e6887'>" + issues.getJSONObject(i).getString("key") + "</span></strong>" + " " 
								+ summary + "</p>" + " " + "<p style='float:right;width:40%'>" + " &nbsp; (" + storyPoint + "pt) " + "<span style='float:right;color:#7e8188'>" 
								+ assigned + "</span>" + "</p>";
						
						active.add(display);
					}
					catch(Exception e)
					{
						assigned = "Unassigned";
						display = "<p style='float:left;width:60%'><strong><span style='color:#ed5624'>" + issues.getJSONObject(i).getString("key") + "</span></strong>" + " " 
								+ summary + "</p>" + " " + "<p style='float:right;width:40%'>" + " &nbsp; (" + storyPoint + "pt) " 
								+ "<strong style='float:right'><span style='color:#b21909'>" + " " + assigned + "</span></strong>" + "</p>";
						active.add(0,display);
					}
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return active.toString();
	}
	public String getCompleted()
	{
		try {
			JSONObject stories = new JSONObject(result);
			JSONArray issues = stories.getJSONArray("issues");

			completed.clear();
			String display = null;
			String story = null;
			String status = null;
			String summary = null;
			String assigned = null;
			int storyPoint = 0;
			
			for(int i = 0; i < issues.length(); i++)
			{
				story = issues.getJSONObject(i).getJSONObject("fields").getJSONObject("issuetype").getString("name");
				status = issues.getJSONObject(i).getJSONObject("fields").getJSONObject("status").getString("name");
				summary = issues.getJSONObject(i).getJSONObject("fields").getString("summary");
				
				if(story.equals("Story") && status.equals("Done Done"))
				{
					try
					{
						assigned = issues.getJSONObject(i).getJSONObject("fields").getJSONObject("assignee").getString("displayName");
						storyPoint = issues.getJSONObject(i).getJSONObject("fields").getInt("customfield_10002");
						display = "<p style='float:left;width:60%'><strong><span style='color:#3e6887'>" + issues.getJSONObject(i).getString("key") + "</span></strong>" + " " 
								+ summary + "</p>" + " " + "<p style='float:right;width:40%'>" + " &nbsp; (" + storyPoint + "pt) " + "<span style='float:right;color:#7e8188'>" 
								+ assigned + "</span>" + "</p>";
						completed.add(display);
					}
					catch(Exception e)
					{
						assigned = "Unassigned";
						display = "<p style='float:left;width:60%'><strong><span style='color:#ed5624'>" + issues.getJSONObject(i).getString("key") + "</span></strong>" + " " 
								+ summary + "</p>" + " " + "<p style='float:right;width:40%'>" + " &nbsp; (" + storyPoint + "pt) " 
								+ "<strong style='float:right'><span style='color:#b21909'>" + " " + assigned + "</span></strong>" + "</p>";
						completed.add(0,display);
					}
				}
			}
		
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
			return completed.toString();
	}
	
	public int getStoryPointsActive()
	{
		int storyPoints = 0;
		
		try {
			JSONObject stories = new JSONObject(result);
			JSONArray issues = stories.getJSONArray("issues");

			String story = null;
			String status = null;
			storyPoints = 0;
			
			for(int i = 0; i < issues.length(); i++)
			{
				story = issues.getJSONObject(i).getJSONObject("fields").getJSONObject("issuetype").getString("name");
				status = issues.getJSONObject(i).getJSONObject("fields").getJSONObject("status").getString("name");
				
				if(story.equals("Story") && !status.equals("Done Done"))
				{
					try
					{
						storyPoints += issues.getJSONObject(i).getJSONObject("fields").getInt("customfield_10002");
					}
					catch(Exception e)
					{
						
					}
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return storyPoints;
	}
	public int getStoryPointsComplete()
	{
		int storyPoints = 0;
		
		try {
			JSONObject stories = new JSONObject(result);
			JSONArray issues = stories.getJSONArray("issues");

			String story = null;
			String status = null;
			storyPoints = 0;
			
			for(int i = 0; i < issues.length(); i++)
			{
				story = issues.getJSONObject(i).getJSONObject("fields").getJSONObject("issuetype").getString("name");
				status = issues.getJSONObject(i).getJSONObject("fields").getJSONObject("status").getString("name");
				
				if(story.equals("Story") && status.equals("Done Done"))
				{
					try
					{
						storyPoints += issues.getJSONObject(i).getJSONObject("fields").getInt("customfield_10002");
					}
					catch(Exception e)
					{
						
					}
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return storyPoints;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	public String getName()
	{
		return name;
	}
	public void setPassword(String password)
	{
		this.password = password;
	}
	public String getPassword()
	{
		return password;
	}
	public void setSprint(String sprint)
	{
		this.sprint = sprint;
	}
	public String getSprint()
	{
		return sprint;
	}
	
	public double getTime() throws ParseException
	{
		double time = 0.0;
		String date = null;
		
		try {
			JSONObject stories = new JSONObject(result);
			JSONArray issues = stories.getJSONArray("issues");

			String story = null;
			String status = null;
			String data = null;
			
			for(int i = 0; i < issues.length(); i++)
			{
				story = issues.getJSONObject(i).getJSONObject("fields").getJSONObject("issuetype").getString("name");
				status = issues.getJSONObject(i).getJSONObject("fields").getJSONObject("status").getString("name");
				
				if(story.equals("Story") && !status.equals("Done Done"))
				{
					try
					{
						data = issues.getJSONObject(i).getJSONObject("fields").getJSONArray("customfield_10303").getString(0);
						break;
					}
					catch(Exception e){
					}
				}
			}
			
			String delim = "[,]";
			String[] tokens = data.split(delim);
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			
			for(int i = 0; i < tokens.length; i++)
			{
				if(tokens[i].contains("endDate"))
				{
					date = tokens[i].substring(8, 18);
				}
			}
			
			Date sprint = format.parse(date);
			Date now = new Date();
	
			double totalTime = 336;
			double sprintTime = (sprint.getTime()/3600000);
			double nowTime = (now.getTime()/3600000);
	
			time = sprintTime - nowTime;
			days = time / 24.0;
			time = ((time / totalTime) *100);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return (100.0 - time);
	}

	public double getDays()
	{
		System.out.println(days);
		return days;
	}
}
