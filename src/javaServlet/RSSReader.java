package javaServlet;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.json.*;

public class RSSReader 
{
	private static GetConfig getConfigData = new GetConfig();
	private static String jenkinsApiUrl = getConfigData.getJenkinsUrl();
	
	@SuppressWarnings("finally")
	public static String readRSS() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException
	{
		String data = null;

		ArrayList<String> status = new ArrayList<String>();
		
		SSLContextBuilder builder = new SSLContextBuilder();
		builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
		SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build());
		CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build();

		HttpGet httpGet = new HttpGet(jenkinsApiUrl);
		CloseableHttpResponse response = httpclient.execute(httpGet);
		try 
		{
			status.add("");
			HttpEntity entity = response.getEntity();
			Header[] headers = response.getAllHeaders();
			for (int i = 0; i < headers.length; i++) 
			{
				System.out.println(headers[i]);
			}
			
			data = EntityUtils.toString(entity);
			
			JSONObject json = new JSONObject(data);
			JSONArray jobs = json.getJSONArray("jobs");
			
			for (int i = 0; i < jobs.length(); i++)
			{
				String name = jobs.getJSONObject(i).getString("name");
				if(name.equals(getConfigData.getServer("1")))
				{
					status.add(jobs.getJSONObject(i).getString("name"));
					status.add(jobs.getJSONObject(i).getString("color"));
				}
				if(name.equals(getConfigData.getServer("2")))
				{
					status.add(jobs.getJSONObject(i).getString("name"));
					status.add(jobs.getJSONObject(i).getString("color"));
				}
				if(name.equals(getConfigData.getServer("3")))
				{
					status.add(jobs.getJSONObject(i).getString("name"));
					status.add(jobs.getJSONObject(i).getString("color"));

				}
				if(name.equals(getConfigData.getServer("4")))
				{
					status.add(jobs.getJSONObject(i).getString("name"));
					status.add(jobs.getJSONObject(i).getString("color"));
				}
				if(name.equals(getConfigData.getServer("5")))
				{
					status.add(jobs.getJSONObject(i).getString("name"));
					status.add(jobs.getJSONObject(i).getString("color"));				
				}
				if(name.equals(getConfigData.getServer("6")))
				{
					status.add(jobs.getJSONObject(i).getString("name"));
					status.add(jobs.getJSONObject(i).getString("color"));
				}
			}
			
			EntityUtils.consume(entity);
			//System.out.println(status.toString());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally 
		{
			response.close();
			return status.toString();
		}
	}
}
